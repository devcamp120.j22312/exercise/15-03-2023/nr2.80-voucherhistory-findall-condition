// Khai báo thư viện express
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

// Khai báo app
const app = express();

// Khai báo port
const port = 8000;

// Import Model
const userModel = require('./app/models/userModel');
const diceHistoryModel = require('./app/models/diceHistoryModel');
const prizeHistoryModel = require('./app/models/prizeHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const voucherHistoryModel = require('./app/models/voucherHistoryModel');
const voucherModel = require('./app/models/voucherModel');

// Cấu hình request đọc được body json
app.use(express.json());

app.use(express.static(__dirname + '/views'))

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

app.get('/', (request, response, next) => {
    const randomNumber = Math.floor((Math.random() * 6) + 1);
    console.log(randomNumber);
    next();
})


app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + "/views/Task 31.30.html"));
})

// Connect MongoDB
mongoose.connect("mongodb://127.0.0.1/LuckyDice_DataBase", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB Successfully!");
})

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})