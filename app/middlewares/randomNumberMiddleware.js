const getCurrentTime  = (request, response, next) =>{
    console.log("Middleware Current time: ", new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }));
    console.log("GET method")
    next();
}
const postMethod = (request, response, next) =>{
    console.log("POST method");
    next();
}
const putMethod = (request, response, next) =>{
    console.log("PUT method");
    next();
}
const deleteMethod = (request, response, next) =>{
    console.log("DELETE method");
    next();
}
module.exports = {
    getCurrentTime,
    postMethod,
    putMethod,
    deleteMethod
}
