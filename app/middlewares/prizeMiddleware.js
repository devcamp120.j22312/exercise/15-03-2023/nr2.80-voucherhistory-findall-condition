const createPrizeMiddleware = (request, response, next) => {
    console.log("Create Prize Middleware");
    next();
}
const getAllPrizeMiddleware = (request, response, next) => {
    console.log("Get All Prize Middleware");
    next();
}
const getDetailPrizeMiddleware = (request, response, next) => {
    console.log("Get Detail Prize Middleware");
    next();
} 
const updatePrizeMiddleware = (request, response, next) => {
    console.log("Update Prize Middleware");
    next();
}
const deletePrizeMiddleware = (request, response, next) => {
    console.log("Delete Prize Middleware");
    next();
}
module.exports = {
    createPrizeMiddleware,
    getAllPrizeMiddleware,
    getDetailPrizeMiddleware,
    updatePrizeMiddleware,
    deletePrizeMiddleware
}