const createDiceHistoryMiddleware = (request, response, next) => {
    console.log("Create DiceHistory Middleware");
    next();
}
const getAllDiceHistoryMiddleware = (request, response, next) => {
    console.log("Get All DiceHistory Middleware");
    next();
}
const getDetailDiceHistoryMiddleware = (request, response, next) => {
    console.log("Get Detail DiceHistory Middleware");
    next();
} 
const updateDiceHistoryMiddleware = (request, response, next) => {
    console.log("Update DiceHistory Middleware");
    next();
}
const deleteDiceHistoryMiddleware = (request, response, next) => {
    console.log("Delete DiceHistory Middleware");
    next();
}
module.exports = {
    createDiceHistoryMiddleware,
    getAllDiceHistoryMiddleware,
    getDetailDiceHistoryMiddleware,
    updateDiceHistoryMiddleware,
    deleteDiceHistoryMiddleware
}