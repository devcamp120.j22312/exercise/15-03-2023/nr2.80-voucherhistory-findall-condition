// Khai báo thư viện
const express = require('express');

// Import prize Controller
const prizeController = require('../controllers/prizeController');

// Import prize Middleware
const prizeMiddleware = require('../middlewares/prizeMiddleware');

// Khai báo router
const prizeRouter = express.Router();

// Create User
prizeRouter.post('/prizes', prizeMiddleware.createPrizeMiddleware, prizeController.createPrize);

// Get All User
prizeRouter.get('/prizes', prizeMiddleware.getAllPrizeMiddleware, prizeController.getAllPrize);

// Get User By Id
prizeRouter.get('/prizes/:prizeId', prizeMiddleware.getDetailPrizeMiddleware, prizeController.getPrizeById);

// Update User By Id
prizeRouter.put('/prizes/:prizeId', prizeMiddleware.updatePrizeMiddleware, prizeController.updatePrizeById);

// Delete User By Id
prizeRouter.delete('/prizes/:prizeId', prizeMiddleware.deletePrizeMiddleware, prizeController.deletePrize);

module.exports = {
    prizeRouter
}