// 
const express = require('express');

// Import Dice History Controller
const diceHistoryController = require('../controllers/diceHistoryController');

// Import Dice History Middleware
const diceHistoryMiddleware = require('../middlewares/diceHistoryMiddleware');

// Khai báo router
const diceHistoryRouter = express.Router();

// Create Dice History
diceHistoryRouter.post('/dice-histories', diceHistoryMiddleware.createDiceHistoryMiddleware, diceHistoryController.createDiceHistory);

// Get All User
diceHistoryRouter.get('/dice-histories', diceHistoryMiddleware.getAllDiceHistoryMiddleware, diceHistoryController.getAllDiceHistory);

// Get User By Id
diceHistoryRouter.get('/dice-histories/:diceHistoryId', diceHistoryMiddleware.getDetailDiceHistoryMiddleware, diceHistoryController.getDiceHistoryById);

// Update User By Id
diceHistoryRouter.put('/dice-histories/:diceHistoryId', diceHistoryMiddleware.updateDiceHistoryMiddleware, diceHistoryController.updateDiceHistoryById);

// Delete User By Id
diceHistoryRouter.delete('/dice-histories/:diceHistoryId', diceHistoryMiddleware.deleteDiceHistoryMiddleware, diceHistoryController.deleteDiceHistory);

module.exports = {
    diceHistoryRouter
}