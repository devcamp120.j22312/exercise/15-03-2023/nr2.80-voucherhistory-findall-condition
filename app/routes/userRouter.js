// Khai báo thư viện
const express = require('express');

// Import user Controller
const userController = require('../controllers/userController');

// Import user Middleware
const userMiddleware = require('../middlewares/userMiddleware');

// Khai báo router
const userRouter = express.Router();

// Create User
userRouter.post('/users', userMiddleware.createUserMiddleware, userController.createUser);

// Get All User
userRouter.get('/users', userMiddleware.getAllUserMiddleware, userController.getAllUser);

// Get User By Id
userRouter.get('/users/:userId', userMiddleware.getDetailUserMiddleware, userController.getUserById);

// Update User By Id
userRouter.put('/users/:userId', userMiddleware.updateUserMiddleware, userController.updateUserById);

// Delete User By Id
userRouter.delete('/users/:userId', userMiddleware.deleteUserMiddleware, userController.deleteUser);

module.exports = {
    userRouter
}