// Khai báo thư viện
const express = require('express');

// Import user Controller
const voucherController = require('../controllers/voucherController');

// Import voucher Middleware
const voucherMiddleware = require('../middlewares/voucherMiddleware');

// Khai báo router
const voucherRouter = express.Router();

// Create User
voucherRouter.post('/vouchers', voucherMiddleware.createVoucherMiddleware, voucherController.createVoucher);

// Get All User
voucherRouter.get('/vouchers', voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher);

// Get User By Id
voucherRouter.get('/vouchers/:voucherId', voucherMiddleware.getDetailVoucherMiddleware, voucherController.getVoucherById);

// Update User By Id
voucherRouter.put('/vouchers/:voucherId', voucherMiddleware.updateVoucherMiddleware, voucherController.updateVoucherById);

// Delete User By Id
voucherRouter.delete('/vouchers/:voucherId', voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucher);

module.exports = {
    voucherRouter
}