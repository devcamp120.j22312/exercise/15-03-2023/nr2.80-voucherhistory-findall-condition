//khai báo express Js
const express = require("express");

//khai báo router
const router = express.Router();
//import middleware
const randomNumberMiddleware = require("../middlewares/randomNumberMiddleware")

router.get("/", randomNumberMiddleware.getCurrentTime,(request, response)=>{
    return response.status(200).json({
        message: "App gets successfull",
    })
});
router.post("/",randomNumberMiddleware.postMethod,(request,response)=>{
    return response.status(200).json({
        message: "App post successfull",
    })
})
router.put("/",randomNumberMiddleware.putMethod,(request,response)=>{
    return response.status(200).json({
        message: "App put successfull",
    })
});
router.delete("/",randomNumberMiddleware.deleteMethod,(request,response)=>{
    return response.status(200).json({
        message: "App delete successfull",
    })
});
module.exports = router;

