const mongoose = require('mongoose');

// 
const Schema = mongoose.Schema;

//
const voucherHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("Voucher History", voucherHistorySchema);