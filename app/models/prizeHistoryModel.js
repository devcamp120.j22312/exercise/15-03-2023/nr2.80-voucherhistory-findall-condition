// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// 
const Schema = mongoose.Schema;

const prizeHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    prize: {
        type: mongoose.Types.ObjectId,
        ref: "Prize",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("Prize History", prizeHistorySchema);