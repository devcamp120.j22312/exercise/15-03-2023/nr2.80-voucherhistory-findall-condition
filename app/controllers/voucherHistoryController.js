const mongoose = require('mongoose');

const voucherHistoryModel = require('../models/voucherHistoryModel');

// Create Voucher History
const createVoucherHistory = (request, response) => {
    const body = request.body;

    const newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        voucher: mongoose.Types.ObjectId(),
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: 'Create new voucher history successfully',
                data: data
            })
        }
    })
}

// Get All Voucher History
const getAllVoucherHistory = (request, response) => {
    let userId = request.query.user;
    let condition = {};
    if (userId) {
        condition.user = userId;
    }
    voucherHistoryModel.find(condition)
        .exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get all Voucher histories successfully',
                data: data
            })
        }
    })
}

// Get Voucher history by id
const getVoucherHistoryById = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Voucher History Id is invalid'
        })
    }
    voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get Voucher history by id successfully',
                data: data
            })
        }
    })
}

const updateVoucherHistoryById = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Voucher History ID is invalid'
        })
    }
    const updateVoucherHistory = {}
    if (body.user !== undefined) {
        updateVoucherHistory.user = mongoose.Types.ObjectId();
    }
    if (body.Voucher !== undefined) {
        updateVoucherHistory.voucher = mongoose.Types.ObjectId();
    }
    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, updateVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: 'Update Voucher History Successfully',
                data: data
            })
        }
    })
}

const deleteVoucherHistoryById = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;

    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Voucher History Id is invalid'
        })
    }
    voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: 'Successfully Deleted!'
            })
        }
    })
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}