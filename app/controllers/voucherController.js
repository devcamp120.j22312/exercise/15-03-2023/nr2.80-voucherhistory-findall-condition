//
const mongoose = require('mongoose');

// 
const voucherModel = require('../models/voucherModel');

// Create Voucher
const createVoucher = (request, response) => {
    const body = request.body;

    //
    if (!body.code) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Code is required'
        })
    }
    if (!body.discount) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Discount is required'
        })
    }
    if (!body.note) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Note is required'
        })
    }
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
        createdAt: body.createdAt,
        updatedAt: body.updateddAt
    }
    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All Voucher
const getAllVoucher = (request, response) => {
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Voucher!',
                data: data
            })
        }
    })
}

// Get Voucher By Id
const getVoucherById = (request, response) => {
    const voucherId = request.params.voucherId;

    // 
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'VoucherId is invalid'
        })
    }
    //
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Voucher By Id',
                data: data
            })
        }
    })
}

// Update
const updateVoucherById = (request, response) => {
    const voucherId = request.params.voucherId;
    const body = request.body;

    // Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'VoucherId is invalid'
        })
    }
    if (!body.code) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Code is required'
        })
    }
    if (!body.discount) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Discount is required'
        })
    }
    if (!body.note) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Note is required'
        })
    }
    let updateVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note,
        createdAt: body.createdAt,
        updatedAt: body.updateddAt
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated',
                data: data
            })
        }
    })
}

// Deleted
const deleteVoucher = (request, response) => {
    const voucherId = request.params.voucherId;

    // 
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'VoucherId is invalid'
        })
    }
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted',
                data: data
            })
        }
    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucher
}