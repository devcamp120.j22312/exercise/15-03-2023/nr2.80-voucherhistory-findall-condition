// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Model
const diceHistoryModel = require('../models/diceHistoryModel');

//  create Dice History
const createDiceHistory = (request, response) => {
    let body = request.body;

    //
    if (!body.user) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User is required'
        })
    }
    if (!body.dice) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User is required'
        })
    }
    //
    let newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: body.dice,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    diceHistoryModel.create(newDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All Dice History
const getAllDiceHistory = (request, response) => {
    const userId = request.query.user;
    const condition = {};
    if (userId) {
        condition.user = userId;
    }
    console.log(condition);

    diceHistoryModel.find(condition)
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Successfully Get All Data',
                    data: data
                })
            }
        })
}

// Get Dice History By Id
const getDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }

    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get User',
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }
    if (!body.user) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User is required'
        })
    }
    if (!body.dice) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User is required'
        })
    }
    let updateDiceHistory = {
        user: body.user,
        dice: body.dice,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deleteDiceHistory = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistory
}