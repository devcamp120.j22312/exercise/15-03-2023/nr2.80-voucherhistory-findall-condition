const mongoose = require('mongoose');

const prizeHistoryModel = require('../models/prizeHistoryModel');

// Create Prize History
const createPrizeHistory = (request, response) => {
    const body = request.body;

    const newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: 'Create new prize history successfully',
                data: data
            })
        }
    })
}

// Get All Prize History
const getAllPrizeHistory = (request, response) => {
    const userId = request.query.user;
    let condition = {};
    if (userId) {
        condition.user = userId;
    }

    prizeHistoryModel.find(condition)
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Get all prize histories successfully',
                    data: data
                })
            }
        })
}

// Get Prize history by id
const getPrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Prize History Id is invalid'
        })
    }
    prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Get prize history by id successfully',
                data: data
            })
        }
    })
}

const updatePrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.params.prizeHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Prize History ID is invalid'
        })
    }
    const updatePrizeHistory = {}
    if (body.user !== undefined) {
        updatePrizeHistory.user = mongoose.Types.ObjectId();
    }
    if (body.prize !== undefined) {
        updatePrizeHistory.prize = mongoose.Types.ObjectId();
    }
    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, updatePrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: 'Update Prize History Successfully',
                data: data
            })
        }
    })
}

const deletePrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Prize History Id is invalid'
        })
    }
    prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: 'Successfully Deleted!'
            })
        }
    })
}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}